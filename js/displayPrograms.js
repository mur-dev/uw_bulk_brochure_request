/**
 * @file
 * Javascript for request brochure
 */

(function ($) {
	Drupal.behaviors.exampleModule = {
	attach: function (context, settings) {
		$('#expand').click(function () {
			$('#expanded').slideToggle("fast");
		});
		$('#bundles').click(function () {
			$('#expand-bundles').slideToggle("fast");
		});
		$('form').submit(function() {
			$('.form-submit').prop("disabled", true);
		})
	}
	};
})(jQuery);
